package com.vnb.banksystem.controller;

import com.vnb.banksystem.common.Msg;
import com.vnb.banksystem.mapper.UserMapper;
import com.vnb.banksystem.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.MapUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author wxj
 * @date 2022-07-11
 */

@RestController
@RequestMapping("api/verify")
public class LoginController {

    private final HttpServletRequest request;

    @Autowired
    private UserService userService;

    @Autowired
    public LoginController(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * 登录验证
     *
     * @param map
     * @return
     */
    @ApiOperation(value = "登录验证")
    @PostMapping(value = "/login")
    @ApiImplicitParam(name = "map", value = "{'name':'用户名','password':'密码'}")
    public Msg login(@RequestBody Map map) {
        if (MapUtils.isEmpty(map)) {
            return Msg.fail().add("error", "请输入信息后重试！");
        }
        Map login = userService.selectByNameAndPassword(map);
        if (login != null) {
            if (login.get("status").equals(1)) {
                return Msg.fail().add("error", "账号已锁定");
            }
            Subject subject = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(login.get("name").toString(), login.get("password").toString());
            try {
                subject.login(token);
                return Msg.success().add("login", login);
            } catch (UnknownAccountException | IncorrectCredentialsException e) {
                return Msg.fail();
            }
        }
        return Msg.fail().add("error", "用户名密码错误");
    }


    /**
     * 注册新用户
     *
     * @param map
     * @return
     */
    @ApiOperation(value = "注册新用户")
    @PostMapping(value = "/register")
    @ApiImplicitParam(name = "map", value = "{'name':'用户名','password':'密码','phone':'手机号'}")
    public Msg register(@RequestBody Map map) {
        if (MapUtils.isEmpty(map)) {
            return Msg.fail().add("error", "请输入信息后重试！");
        }
        Map user = userService.selectByName(map.get("name").toString());
        if (user != null) {
            return Msg.fail().add("error", "注册失败，用户名已存在！");
        }
        Integer addUser = userService.insertSelective(map);
        if (addUser > 0) {
            return Msg.success().add("msg", "注册成功！");
        }
        return Msg.fail().add("error", "未知异常，请重试！");
    }


    /**
     * 添加新用户或管理员
     *
     * @param map
     * @return
     */
    @ApiOperation(value = "添加新用户")
    @PostMapping(value = "/addUser")
    @ApiImplicitParam(name = "map", value = "{'name':'用户名','password':'密码','phone':'手机号','roleId':'角色id:1->user,2->admin设置下拉或单选按钮'}")
    @RequiresRoles("admin")
    @RequiresPermissions("user:add")
    public Msg addUser(@RequestBody Map map) {
        if (MapUtils.isEmpty(map)) {
            return Msg.fail().add("error", "请输入信息后重试！");
        }
        Map user = userService.selectByName(map.get("name").toString());
        if (user != null) {
            return Msg.fail().add("error", "添加失败，用户名已存在！");
        }
        Integer addUser = userService.insertSelective(map);
        if (addUser > 0) {
            return Msg.success().add("msg", "添加成功！");
        }
        return Msg.success();
    }


    /**
     * 完善用户信息
     *
     * @param map
     * @return
     */
    @ApiOperation(value = "完善用户信息")
    @PostMapping(value = "/completeInfo")
    @ApiImplicitParam(name = "map",
            value = "{'realName':'真实姓名','certificateType':'证件类型','certificateNumber':'证件号','address':'联系地址','email':'邮箱'}")
    @RequiresRoles("user")
    @RequiresPermissions("user:select")
    public Msg completeInfo(@RequestBody Map map) {
        if (MapUtils.isEmpty(map)) {
            return Msg.fail().add("error", "请输入信息后重试！");
        }
        Integer userId = getUserId();
        if (userId == 0) {
            return Msg.fail().add("error", "身份信息失效，请重新登录！");
        }
        map.put("userId", userId.toString());
        Integer user = userService.updateUserInfo(map);
        if (user > 0) {
            return Msg.success().add("msg", "用户信息完善成功");
        }
        return Msg.fail().add("error", "用户信息完善失败，请重试！");
    }


    /**
     * 更新用户信息
     *
     * @param map
     * @return
     */
    @ApiOperation(value = "更新用户信息")
    @PostMapping(value = "/updateUserInfo")
    @ApiImplicitParam(name = "map",
            value = "{'password':'密码','phone':'手机号','roleId':'角色id:1->user,2->admin设置下拉或单选按钮','realName':'真实姓名','certificateType':'证件类型','certificateNumber':'证件号','address':'联系地址','email':'邮箱'}")
    @RequiresRoles("user")
    @RequiresPermissions("user:select")
    public Msg updateUserInfo(@RequestBody Map map) {
        if (MapUtils.isEmpty(map)) {
            return Msg.fail().add("error", "请输入信息后重试！");
        }
        Integer userId = getUserId();
        if (userId == 0) {
            return Msg.fail().add("error", "身份信息失效，请重新登录！");
        }
        map.put("userId", userId.toString());
        Integer user = userService.updateUserInfo(map);
        if (user > 0) {
            return Msg.success().add("msg", "用户信息更新成功");
        }
        return Msg.fail().add("error", "用户信息更新失败，请重试！");
    }


    /**
     * 获取当前用户id
     *
     * @return
     */
    private Integer getUserId() {
        Map loginUser = (Map) request.getSession().getAttribute("loginUser");
        Map loginAdmin = (Map) request.getSession().getAttribute("loginAdmin");
        Integer id = 0;
        if (loginAdmin != null) {
            id = Integer.parseInt(loginAdmin.get("id").toString());
        }
        if (loginUser != null) {
            id = Integer.parseInt(loginUser.get("id").toString());
        }
        return id;
    }


    /**
     * 未登录返回登录信息
     *
     * @return
     */
    @ApiOperation(value = "未登录返回登录信息")
    @GetMapping("/toLogin")
    public Msg toLogin() {
        return Msg.fail().add("error", "请先登录！");
    }


    /**
     * 注销
     *
     * @return
     */
    @ApiOperation(value = "注销")
    @GetMapping("/loginOut")
    public Msg loginOut() {
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.logout();
            System.out.println("您已退出系统");
            return Msg.success().add("loginOut", "您已成功退出");
        } catch (UnknownAccountException | IncorrectCredentialsException e) {
            return Msg.fail().add("error", "退出异常请重试");
        }
    }
}
