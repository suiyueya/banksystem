package com.vnb.banksystem.controller;

import com.vnb.banksystem.common.Msg;
import com.vnb.banksystem.domain.TransactionDetail;
import com.vnb.banksystem.domain.vo.OrderVo;
import com.vnb.banksystem.service.TransactionDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author sy
 */
@Api("交易接口")
@RestController
@RequestMapping("api/transaction")
public class TransactionController {

    @Autowired
    private TransactionDetailService transactionDetailService;

    /**
     * 管理员查询某个或多个交易接口
     * @param transactionDetail
     * @return
     */
    @ApiOperation("管理员查询某个或多个交易")
    @ApiImplicitParam(name = "transactionDetail", value = "封装查询参数，根据输入条件查询")
    @GetMapping("/list")
    public Msg list(TransactionDetail transactionDetail) {

        return Msg.success();
    }

    /**
     * 转账接口
     * @param orderVo
     * @return
     */
    @ApiOperation("转账")
    @ApiImplicitParam(name = "orderVo", value = "封装查询参数，必要参数为付款人名、密码、卡号、交易金额；收款人银行卡号、账户名")
    @PostMapping("/transfer")
    public Msg mqTransfer(@RequestBody @Validated OrderVo orderVo) {

        return Msg.success();
    }

    /**
     * 个人交易查询接口
     * @param transactionDetail
     * @return
     */
    @ApiOperation("个人交易查询")
    @ApiImplicitParam(name = "transactionDetail", value = "封装查询参数，根据输入条件查询")
    @GetMapping("/selectByName")
    public Msg selectByName(TransactionDetail transactionDetail) {

        return Msg.success();
    }

    /**
     * 充值接口
     * @param orderVo
     * @return
     */
    @ApiOperation("充值")
    @ApiImplicitParam(name = "orderVo", value = "封装查询参数，必要参数为卡号(payCardNumber)、交易金额(transactionAmount)")
    @PostMapping("/recharge")
    public Msg recharge(@RequestBody OrderVo orderVo) {
        if (orderVo.getTransactionAmount() == null || orderVo.getPayCardNumber() == null){
            return Msg.fail();
        }
        return Msg.success();
    }

    /**
     * 提现接口
     * @param orderVo
     * @return
     */
    @ApiOperation("提现")
    @ApiImplicitParam(name = "orderVo", value = "封装查询参数，必要参数为卡号(payCardNumber)、交易金额(transactionAmount)、密码(password)、账户名(payName)")
    @PostMapping("/withdraw")
    public Msg withdraw(@RequestBody OrderVo orderVo) {
        if (orderVo.getPassword() == null || orderVo.getTransactionAmount() == null || orderVo.getPayCardNumber() == null || orderVo.getPayName() == null){
            return Msg.fail();
        }
        return Msg.success();
    }
}
