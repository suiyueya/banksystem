package com.vnb.banksystem.controller;

import com.vnb.banksystem.common.Msg;
import com.vnb.banksystem.domain.Account;
import com.vnb.banksystem.domain.vo.AccountInformation;
import com.vnb.banksystem.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/account")
@Api("账户接口")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @ApiOperation("根据账户id查询账户信息")
    @ApiImplicitParam(name = "accountId",value = "账户id")
    @GetMapping("getAccountByID/{accountId}")
    public Msg getAccountById(@PathVariable int accountId){
        Account account = accountService.getById(accountId);
        return Msg.success().add("account",account);
    }

    @ApiOperation("开户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",value = "账户名"),
            @ApiImplicitParam(name = "password",value = "交易密码"),
            @ApiImplicitParam(name = "bankName",value = "开户行"),
            @ApiImplicitParam(name = "cardNumber",value = "银行卡号"),
    })
    @PostMapping("addAccount")
    public Msg addAccount(@RequestBody AccountInformation information){
        accountService.addAccount(information);
        return Msg.success();
    }

    @ApiOperation("修改账户信息")
    @PostMapping("updateAccount")
    //@ApiImplicitParam(name = "account",value = "账户信息")
    public Msg updateAccount(@RequestBody Account account){
        accountService.updateAccount(account);
        return Msg.success();
    }

    @ApiOperation("根据需求查询用户信息")
    @PostMapping("getAccountByDemand")
    @ApiImplicitParam(name = "map",value = "{'name':'账户名','card_number':'银行卡号','bank_name':'开户行'}")
    public Msg getAccountByDemand(@RequestBody Map map){
        List<Account> accountByDemand = accountService.getAccountByDemand(map);
        return Msg.success().add("date",accountByDemand);
    }
}
