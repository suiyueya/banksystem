package com.vnb.banksystem.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @BelongsProject: banksystem
 * @BelongsPackage: com.vnb.banksystem.controller
 * @Author: gy
 * @CreateTime: 2022-07-11  15:39
 * @Description:
 * @Version: 1.0
 */
@Api("报表导入导出接口")
@RestController
@RequestMapping(value = "excel")
public class ExcelController {
    /**
     *  导入
     **/
    @ApiOperation(value = "导入数据")
    @PostMapping("/importExcel")
    public int importData(MultipartFile file) throws Exception {

        return 0;
    }

    /**
     * 导出
     **/
    @ApiOperation(value = "导出数据",produces = "application/octet-stream")
    @GetMapping("/exportExcel")
    public void exportData(HttpServletResponse response) throws IOException {

    }
}