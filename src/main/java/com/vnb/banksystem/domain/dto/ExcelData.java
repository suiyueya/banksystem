package com.vnb.banksystem.domain.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.vnb.banksystem.common.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @BelongsProject: banksystem
 * @BelongsPackage: com.vnb.banksystem.domain.dto
 * @Author: gy
 * @CreateTime: 2022-07-11  14:41
 * @Description: Excel表
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@HeadRowHeight(value = 40)
public class ExcelData {
    @Excel(name = "付款人")
    @ColumnWidth(15)
    private String payName;
    @Excel(name = "付款人账户")
    @ColumnWidth(20)
    private String payCardNumber;
    @Excel(name = "付款人开户行")
    @ColumnWidth(15)
    private String payBankName;
    @Excel(name = "收款人")
    @ColumnWidth(15)
    private String receiver;
    @Excel(name = "收款人账户")
    @ColumnWidth(20)
    private String receiverCardNumber;
    @Excel(name = "收款人开户行")
    @ColumnWidth(15)
    private String receiverBankName;
    @Excel(name = "交易金额")
    @ColumnWidth(10)
    private double transactionAmount;
    @Excel(name = "币种")
    @ColumnWidth(5)
    private String currency;
    @Excel(name = "交易类型")
    @ColumnWidth(10)
    private String dealType;
    @Excel(name = "备注")
    @ColumnWidth(20)
    private String remark;
    @Excel(name = "创建时间")
    @ColumnWidth(15)
    private Date createTime;
    @Excel(name = "交易状态")
    @ColumnWidth(5)
    private int status;
    @Excel(name = "异常原因")
    @ColumnWidth(20)
    private String cause;
}