package com.vnb.banksystem.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 账户表
 *
 * @TableName account
 */
@TableName(value = "account")
@Data
@ApiModel(value = "账户",description = "账户表")
public class Account implements Serializable {
    /**
     * 主键id
     */
    @ApiModelProperty(value = "账户id")
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 账户名
     */
    @ApiModelProperty(value = "账户名")
    private String name;

    /**
     * 交易密码
     */
    @ApiModelProperty(value = "交易密码")
    private String password;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value = "银行卡号")
    private String cardNumber;

    /**
     * 开户行
     */
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /**
     * 账户余额
     */
    @ApiModelProperty(value = "账户余额")
    private BigDecimal balance;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 状态（0->正常，1->冻结，2->销户）
     */
    @ApiModelProperty(value = "状态（0->正常，1->冻结，2->销户")
    private Byte status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", password=").append(password);
        sb.append(", cardNumber=").append(cardNumber);
        sb.append(", bankName=").append(bankName);
        sb.append(", balance=").append(balance);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}