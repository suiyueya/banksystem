package com.vnb.banksystem.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 交易表
 *
 * @TableName transaction
 */
@TableName(value = "transaction")
@Data
@ApiModel(value = "交易表")
public class Transaction implements Serializable {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "交易表主键id")
    private Integer id;

    /**
     * 付款人账户id
     */
    @ApiModelProperty(value = "付款人账户id")
    private Integer payAccountId;

    /**
     * 收款人账户id
     */
    @ApiModelProperty(value = "收款人账户id")
    private Integer payeeAccountId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 交易状态（0->成功，1->失败）
     */
    @ApiModelProperty(value = "交易状态（0->成功，1->失败）")
    private Byte status;

    /**
     * 摘要
     */
    @ApiModelProperty(value = "摘要")
    private String remark;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", payAccountId=").append(payAccountId);
        sb.append(", payeeAccountId=").append(payeeAccountId);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}