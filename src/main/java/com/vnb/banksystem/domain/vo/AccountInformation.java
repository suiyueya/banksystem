package com.vnb.banksystem.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel(value = "账户信息")
@Data
public class AccountInformation {

    /**
     * 账户名
     */
    @ApiModelProperty(value = "账户名")
    private String name;

    /**
     * 交易密码
     */
    @ApiModelProperty(value = "交易密码")
    private String password;


    /**
     * 开户行
     */
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value = "银行卡号")
    private String cardNumber;

    /**
     * 创建时间
     *//*
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;*/
}
