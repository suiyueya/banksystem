package com.vnb.banksystem.domain.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Author sy
 * @Date 2022/7/13 10:54
 */
@Data
@ApiModel(value = "交易",description = "交易工具类")
public class OrderVo {
    /**
     * 付款人密码
      */
    @ApiModelProperty(value = "付款人密码")
    @NotNull(message = "密码不能为空")
    private String password;

    /**
     * 付款人
     */
    @ApiModelProperty(value = "付款人")
    @NotNull(message = "付款人不能为空")
    private String payName;

    /**
     * 收款人
     */
    @ApiModelProperty(value = "收款人")
    @NotNull(message = "收款人不能为空")
    private String receiver;

    /**
     * 付款人卡号
     */
    @ApiModelProperty(value = "付款人卡号")
    @NotNull(message = "付款人卡号不能为空")
    private String payCardNumber;

    /**
     * 交易金额
     */
    @ApiModelProperty(value = "交易金额")
    @NotNull(message = "交易金额不能为空")
    private BigDecimal transactionAmount;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 收款人卡号
     */
    @ApiModelProperty(value = "收款人卡号")
    @NotNull(message = "收款人卡号不能为空")
    private String receiverCardNumber;
}
