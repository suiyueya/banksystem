package com.vnb.banksystem.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 交易明细表
 * @TableName transaction_detail
 */
@TableName(value = "transaction_detail")
@Data
@ApiModel("交易明细表")
public class TransactionDetail implements Serializable {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 交易id
     */
    @ApiModelProperty(value = "交易id")
    private Integer transactionId;

    /**
     * 付款人
     */
    @ApiModelProperty(value = "付款人名字")
    private String payName;

    /**
     * 付款人账户（银行卡号）
     */
    @ApiModelProperty(value = "付款人银行卡号")
    private String payCardNumber;

    /**
     * 付款人开户行
     */
    @ApiModelProperty(value = "付款人开户行")
    private String payBankName;

    /**
     * 收款人
     */
    @ApiModelProperty(value = "收款人名字")
    private String receiver;

    /**
     * 收款人账户（银行卡号）
     */
    @ApiModelProperty(value = "收款人银行卡号")
    private String receiverCardNumber;

    /**
     * 收款人开户行
     */
    @ApiModelProperty(value = "收款人开户行")
    private String receiverBankName;

    /**
     * 交易金额
     */
    @ApiModelProperty(value = "交易金额")
    private BigDecimal transactionAmount;

    /**
     * 币种
     */
    @ApiModelProperty(value = "币种")
    private String currency;

    /**
     * 交易类型
     */
    @ApiModelProperty(value = "交易金额")
    private String dealType;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 交易状态（0->成功，1->失败）
     */
    @ApiModelProperty(value = "交易状态")
    private Byte status;

    /**
     * 异常原因
     */
    @ApiModelProperty(value = "异常原因")
    private String cause;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TransactionDetail other = (TransactionDetail) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getTransactionId() == null ? other.getTransactionId() == null : this.getTransactionId().equals(other.getTransactionId()))
                && (this.getPayName() == null ? other.getPayName() == null : this.getPayName().equals(other.getPayName()))
                && (this.getPayCardNumber() == null ? other.getPayCardNumber() == null : this.getPayCardNumber().equals(other.getPayCardNumber()))
                && (this.getPayBankName() == null ? other.getPayBankName() == null : this.getPayBankName().equals(other.getPayBankName()))
                && (this.getReceiver() == null ? other.getReceiver() == null : this.getReceiver().equals(other.getReceiver()))
                && (this.getReceiverCardNumber() == null ? other.getReceiverCardNumber() == null : this.getReceiverCardNumber().equals(other.getReceiverCardNumber()))
                && (this.getReceiverBankName() == null ? other.getReceiverBankName() == null : this.getReceiverBankName().equals(other.getReceiverBankName()))
                && (this.getTransactionAmount() == null ? other.getTransactionAmount() == null : this.getTransactionAmount().equals(other.getTransactionAmount()))
                && (this.getCurrency() == null ? other.getCurrency() == null : this.getCurrency().equals(other.getCurrency()))
                && (this.getDealType() == null ? other.getDealType() == null : this.getDealType().equals(other.getDealType()))
                && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
                && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
                && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
                && (this.getCause() == null ? other.getCause() == null : this.getCause().equals(other.getCause()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTransactionId() == null) ? 0 : getTransactionId().hashCode());
        result = prime * result + ((getPayName() == null) ? 0 : getPayName().hashCode());
        result = prime * result + ((getPayCardNumber() == null) ? 0 : getPayCardNumber().hashCode());
        result = prime * result + ((getPayBankName() == null) ? 0 : getPayBankName().hashCode());
        result = prime * result + ((getReceiver() == null) ? 0 : getReceiver().hashCode());
        result = prime * result + ((getReceiverCardNumber() == null) ? 0 : getReceiverCardNumber().hashCode());
        result = prime * result + ((getReceiverBankName() == null) ? 0 : getReceiverBankName().hashCode());
        result = prime * result + ((getTransactionAmount() == null) ? 0 : getTransactionAmount().hashCode());
        result = prime * result + ((getCurrency() == null) ? 0 : getCurrency().hashCode());
        result = prime * result + ((getDealType() == null) ? 0 : getDealType().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getCause() == null) ? 0 : getCause().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", transactionId=").append(transactionId);
        sb.append(", payName=").append(payName);
        sb.append(", payCardNumber=").append(payCardNumber);
        sb.append(", payBankName=").append(payBankName);
        sb.append(", receiver=").append(receiver);
        sb.append(", receiverCardNumber=").append(receiverCardNumber);
        sb.append(", receiverBankName=").append(receiverBankName);
        sb.append(", transactionAmount=").append(transactionAmount);
        sb.append(", currency=").append(currency);
        sb.append(", dealType=").append(dealType);
        sb.append(", remark=").append(remark);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append(", cause=").append(cause);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}