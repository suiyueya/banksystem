package com.vnb.banksystem.common;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * @author sy
 */
@Configuration
public class RabbitMqConfig {

    /**
     * 定义交换机名称
     */
    public static final String ORDER_CREATE_MSG_EXCHANGE = "order_create_msg_exchange";

    /**
     * 定义两个队列  系统发给转账用户的信息队列，系统发给被转账用户的消息队列
     */
    public static final String MSG_BUYER_QUEUE = "msg_buyer_queue";
    public static final String MSG_SELLER_QUEUE = "msg_seller_queue";

    /**
     * 定义两个routingKey
     */
    public static final String MSG_BUYER_RK = "msg_buyer_rk";
    public static final String MSG_SELLER_RK = "msg_seller_rk";

    /**
     * 声明转账交换机
     * @return
     */
    @Bean
    public DirectExchange orderMsgExchange(){
        return ExchangeBuilder.directExchange(ORDER_CREATE_MSG_EXCHANGE).durable(true).build();
    }

    /**
     * 声明转账队列
     * @return
     */
    @Bean
    public Queue buyerQueue(){
        return QueueBuilder.durable(MSG_BUYER_QUEUE).build();
    }
    @Bean
    public Queue sellerQueue(){
        return QueueBuilder.durable(MSG_SELLER_QUEUE).build();
    }

    /**
     * 完成绑定关系(队列和交换机完成绑定关系)
     * @return
     */
    @Bean
    public Binding directBuyerBinding(){
        return BindingBuilder.bind(buyerQueue()).to(orderMsgExchange()).with(MSG_BUYER_RK);
    }
    @Bean
    public Binding directSellerBinding(){
        return BindingBuilder.bind(sellerQueue()).to(orderMsgExchange()).with(MSG_SELLER_RK);
    }
}
