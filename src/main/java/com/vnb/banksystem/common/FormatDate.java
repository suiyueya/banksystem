package com.vnb.banksystem.common;

import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * @author wxj
 */
public class FormatDate {


    private long utcTime;
    public static final TimeZone BASE_TIMEZONE = TimeZone.getTimeZone("GMT+08:00");
    public static final int[] LEAP_MONTH_LENGTH = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public static final int[] MONTH_LENGTH = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public FormatDate() {
        this(System.currentTimeMillis());
    }

    public FormatDate(long m) {
        utcTime = m;
        utcTime -= utcTime % 1000L;
    }

    public FormatDate(java.sql.Date date) {
        this(date.getTime());
    }

    public FormatDate(java.util.Date date) {
        this(date.getTime());
    }

    public FormatDate(String date) {
        int[] t = internalParse(date);
        utcTime = new GregorianCalendar(t[0], t[1] - 1, t[2], t[3], t[4], t[5]).getTimeInMillis();
    }

    /**
     * 解析成yyyy,MM,dd,HH,mm,ss
     *
     * @param str
     * @return
     */
    static int[] internalParse(String str) {
        if (str == null) {
            throw new IllegalArgumentException("invalid FDateTime: " + str);
        }
        str = str.trim();
        int index = str.indexOf(' ');
        if ((index < 0) || (index > str.length() - 1)) {
            throw new IllegalArgumentException("invalid FDateTime: " + str);
        }
        int[] d = internalParseDate(str);

        int[] t = internalParseTime(str, index + 1);

        int[] a = new int[6];

        System.arraycopy(d, 0, a, 0, d.length);

        System.arraycopy(t, 0, a, d.length, t.length);

        return a;
    }

    @Override
    public String toString() {
        GregorianCalendar baseCalendar = basezoneCalendar();
        return toDateTimeString(baseCalendar.get(1), baseCalendar.get(2) + 1, baseCalendar.get(5), baseCalendar.get(11),
                baseCalendar.get(12), baseCalendar.get(13));
    }

    /**
     * 给日历类设置毫秒数
     *
     * @return
     */
    private GregorianCalendar basezoneCalendar() {
        GregorianCalendar basezoneCalendar = new GregorianCalendar(BASE_TIMEZONE);
        basezoneCalendar.setTimeInMillis(utcTime);
        return basezoneCalendar;
    }

    /**
     * 拼接成yyyy-MM-dd HH:mm:ss
     *
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param minute
     * @param second
     * @return
     */
    static String toDateTimeString(int year, int month, int day, int hour, int minute, int second) {
        StringBuffer sb = new StringBuffer();
        String strYear = String.valueOf(year);
        for (int j = strYear.length(); j < 4; j++) {
            sb.append('0');
        }
        sb.append(strYear).append('-');
        append(sb, month, '-');
        append(sb, day, ' ');
        append(sb, hour, ':');
        append(sb, minute, ':');
        if (second < 10) {
            sb.append('0');
        }
        sb.append(second);
        return sb.toString();
    }

    private static void append(StringBuffer sb, int v, char split) {
        if (v < 10) {
            sb.append('0');
        }
        sb.append(v).append(split);
    }

    public int getYear() {
        return basezoneCalendar().get(1);
    }

    public int getMonth() {
        return basezoneCalendar().get(2) + 1;
    }

    public int getDay() {
        return basezoneCalendar().get(5);
    }

    public int getHour() {
        return basezoneCalendar().get(11);
    }

    public int getMinute() {
        return basezoneCalendar().get(12);
    }

    public int getSecond() {
        return basezoneCalendar().get(13);
    }

    /**
     * 解析成yyyy,MM,dd
     *
     * @param str
     * @return
     */
    static int[] internalParseDate(String str) {
        if (str == null) {
            throw new IllegalArgumentException("invalid date: " + str);
        }
        str = str.trim();
        int spaceIndex = str.indexOf(' ');
        if (spaceIndex > -1) {
            str = str.substring(0, spaceIndex);
        }

        String[] tokens = new String[3];
        StringTokenizer st = new StringTokenizer(str, "-/");
        if (st.countTokens() != 3) {
            throw new IllegalArgumentException("invalid date: " + str);
        }

        int i = 0;
        while (st.hasMoreTokens()) {
            tokens[(i++)] = st.nextToken();
        }
        try {
            int year = Integer.parseInt(tokens[0]);
            int month = Integer.parseInt(tokens[1]);
            if ((month < 1) || (month > 12))//限制月份的范围
            {
                throw new IllegalArgumentException("invalid date: " + str);
            }
            int day = Integer.parseInt(tokens[2]);

            int daymax = isLeapYear(year) ? LEAP_MONTH_LENGTH[(month - 1)] : MONTH_LENGTH[(month - 1)];//当年当月的最大天数

            if ((day < 1) || (day > daymax)) {
                throw new IllegalArgumentException("invalid date: " + str);//限制天数的范围
            }
            return new int[]{year, month, day};
        } catch (Throwable thr) {
            if ((thr instanceof IllegalArgumentException)) {
                throw ((IllegalArgumentException) thr);
            }
            throw new IllegalArgumentException("invalid date: " + str);
        }
    }

    /**
     * 是否闰年
     *
     * @param year
     * @return
     */
    public static boolean isLeapYear(int year) {
        if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) {
            return true;
        }
        return false;
    }

    /**
     * 解析成HH,mm,ss
     *
     * @param time
     * @param idx
     * @return
     */
    static int[] internalParseTime(String time, int idx) {
        if ((time == null) || (time.length() == 0)) {
            throw new IllegalArgumentException("Time can't be empty");
        }
        if (time.length() != 8 + idx) {
            throw new IllegalArgumentException("Time must as format HH:mm:ss, " + time);
        }

        for (int i = idx; i < idx + 8; i++) {
            char c = time.charAt(i);
            if ((i == idx + 2) || (i == idx + 5)) {
                if (c != ':') {
                    throw new IllegalArgumentException("Time must as format HH:mm:ss, " + time);
                }

            } else if ((c < '0') || (c > '9')) {
                throw new IllegalArgumentException("Time must as format HH:mm:ss, " + time);
            }
        }
        int hour = Integer.parseInt(time.substring(idx + 0, idx + 2));
        int minute = Integer.parseInt(time.substring(idx + 3, idx + 5));
        int second = Integer.parseInt(time.substring(idx + 6, idx + 8));
        if ((hour < 0) || (hour >= 24) || (minute < 0) || (minute > 59) || (second < 0) || (second > 59)) {
            throw new IllegalArgumentException("Invalid time, " + time);
        }

        return new int[]{hour, minute, second};
    }


}
