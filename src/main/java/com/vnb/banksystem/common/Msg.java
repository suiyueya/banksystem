package com.vnb.banksystem.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用的返回类（封装json数据）
 *
 * @author admin
 */
public class Msg {


    /**
     * 状态码（1000-成功，2000-失败）
     *
     */
    private int code;
    /**
     *
     *
     */
    private String msg;

    /**
     * 返回数据类型
     *
     */
    private Map<String, Object> extend = new HashMap<String, Object>();

    /**
     * 处理成功
     *
     * @return
     */
    public static com.vnb.banksystem.common.Msg success() {
        com.vnb.banksystem.common.Msg result = new com.vnb.banksystem.common.Msg();
        result.setCode(1000);
		result.setMsg("处理成功！");
		return result;
	}

	/**
	 * 处理失败
	 *
	 * @return
	 */
	public static com.vnb.banksystem.common.Msg fail() {
		com.vnb.banksystem.common.Msg result = new com.vnb.banksystem.common.Msg();
		result.setCode(2000);
		result.setMsg("处理失败！");
		return result;
	}

	/**
	 * 添加要返回的json数据
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public com.vnb.banksystem.common.Msg add(String key, Object value) {
		this.getExtend().put(key, value);
		return this;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Map<String, Object> getExtend() {
		return extend;
	}

	public void setExtend(Map<String, Object> extend) {
		this.extend = extend;
	}
}
