package com.vnb.banksystem.common.shiro;


import com.vnb.banksystem.domain.Permissions;
import com.vnb.banksystem.service.UserPermissionsService;
import com.vnb.banksystem.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.List;
import java.util.Map;


/**
 * @author wxj
 */
public class UserRealm extends AuthorizingRealm {

    @Lazy
    @Autowired
    UserService userService;
    @Lazy
    @Autowired
    UserPermissionsService userPermissionsService;


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {


        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();


        //获取当前登录的对象

        Subject subject = SecurityUtils.getSubject();

        String currentUserUsername = (String) subject.getPrincipal();
        Map user = userService.selectByName(currentUserUsername);
        if (user != null) {
            if ("admin".equals(user.get("roleName"))) {
                info.addRole("user");
                info.addRole("admin");
            }
            if ("user".equals(user.get("roleName"))) {
                info.addRole("user");
            }
            List<Permissions> list = userPermissionsService.selectUserPermissionsByUserId(Integer.parseInt(user.get("id").toString()));
            for (Permissions up : list) {
                info.addStringPermission(up.getPermission());
            }
        }


        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {


        UsernamePasswordToken userToken = (UsernamePasswordToken) token;

        //从token中取到用户名再去查用户密码

        Map user = userService.selectByName(userToken.getUsername());
        if (user != null) {
            Subject currentSubject = SecurityUtils.getSubject();
            Session session = currentSubject.getSession();
            if ("admin".equals(user.get("roleName"))) {
                session.setAttribute("loginAdmin", user);
                System.out.println("认证成功=>管理员：" + user.get("name") + "登录进入系统");
            }
            if ("user".equals(user.get("roleName"))) {
                session.setAttribute("loginUser", user);
                System.out.println("认证成功=>用户：" + user.get("name") + "登录进入系统");
            }
            return new SimpleAuthenticationInfo(user.get("name"), user.get("password"), "");
        }

        return null;
    }
}
