package com.vnb.banksystem.common.shiro;

import com.vnb.banksystem.common.Msg;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author wxj
 */
@ControllerAdvice
public class UnPermissionException {


    @ResponseBody
    @ExceptionHandler(UnauthorizedException.class)
    public Msg handleShiroException() {
        return Msg.fail().add("error", "您没有权限");
    }

    @ResponseBody
    @ExceptionHandler(AuthorizationException.class)
    public Msg authorizationException() {
        return Msg.fail().add("error", "权限认证失败，请联系管理员");
    }
}
