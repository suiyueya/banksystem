package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.Permissions;
import com.vnb.banksystem.domain.UserPermissions;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @author wxj
 */
public interface UserPermissionsService extends IService<UserPermissions> {


    /**
     * 根据用户id查询权限
     *
     * @param id
     * @return
     */
    List<Permissions> selectUserPermissionsByUserId(Integer id);

    /**
     * 添加用户权限
     *
     * @param map
     * @return
     */
    Integer insertUserPermission(Map map);


    /**
     * 根据用户id删除权限
     *
     * @param id
     * @return
     */
    Integer deleteByUserId(Integer id);
}
