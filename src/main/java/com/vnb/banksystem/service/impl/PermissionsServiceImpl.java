package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.domain.Permissions;
import com.vnb.banksystem.service.PermissionsService;
import com.vnb.banksystem.mapper.PermissionsMapper;
import org.springframework.stereotype.Service;

/**
 * @author wxj
 */
@Service
public class PermissionsServiceImpl extends ServiceImpl<PermissionsMapper, Permissions>
        implements PermissionsService {

}




