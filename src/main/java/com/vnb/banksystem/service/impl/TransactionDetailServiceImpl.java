package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.domain.TransactionDetail;
import com.vnb.banksystem.service.TransactionDetailService;
import com.vnb.banksystem.mapper.TransactionDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author sy
 */
@Service
public class TransactionDetailServiceImpl extends ServiceImpl<TransactionDetailMapper, TransactionDetail>
        implements TransactionDetailService {

    @Autowired
    private TransactionDetailMapper transactionDetailMapper;
}




