package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.domain.Permissions;
import com.vnb.banksystem.domain.UserPermissions;
import com.vnb.banksystem.service.UserPermissionsService;
import com.vnb.banksystem.mapper.UserPermissionsMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wxj
 */
@Service
public class UserPermissionsServiceImpl extends ServiceImpl<UserPermissionsMapper, UserPermissions>
        implements UserPermissionsService {

    @Override
    public List<Permissions> selectUserPermissionsByUserId(Integer id) {
        return baseMapper.selectUserPermissionsByUserId(id);
    }

    @Override
    public Integer insertUserPermission(Map map) {
        return baseMapper.insertUserPermission(map);
    }

    @Override
    public Integer deleteByUserId(Integer id) {
        return baseMapper.deleteByUserId(id);
    }
}




