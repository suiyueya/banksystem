package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.domain.Account;
import com.vnb.banksystem.domain.vo.AccountInformation;
import com.vnb.banksystem.service.AccountService;
import com.vnb.banksystem.mapper.AccountMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account>
        implements AccountService {

    //开户
    @Override
    public void addAccount(AccountInformation information) {
        Account account = new Account();
        BeanUtils.copyProperties(information,account);

        //将开户信息存入数据库
        baseMapper.insert(account);

    }

    //修改账户信息
    @Override
    public void updateAccount(Account account) {

        //将修改的账户信息存入数据库
        baseMapper.updateById(account);
    }

    //根据需求查询用户信息
    @Override
    public List<Account> getAccountByDemand(Map map) {
        List<Account> byDemandList = baseMapper.getByDemand(map);
        return byDemandList;
    }


}




