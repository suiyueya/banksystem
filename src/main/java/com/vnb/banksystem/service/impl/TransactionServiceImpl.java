package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.domain.Transaction;
import com.vnb.banksystem.service.TransactionService;
import com.vnb.banksystem.mapper.TransactionMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class TransactionServiceImpl extends ServiceImpl<TransactionMapper, Transaction>
        implements TransactionService {

}




