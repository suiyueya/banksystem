package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.domain.Role;
import com.vnb.banksystem.service.RoleService;
import com.vnb.banksystem.mapper.RoleMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role>
        implements RoleService {

}




