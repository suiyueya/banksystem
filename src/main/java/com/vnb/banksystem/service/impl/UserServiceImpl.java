package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.common.FormatDate;
import com.vnb.banksystem.domain.User;
import com.vnb.banksystem.mapper.RolePermissionsMapper;
import com.vnb.banksystem.mapper.UserMapper;
import com.vnb.banksystem.service.RolePermissionsService;
import com.vnb.banksystem.service.UserPermissionsService;
import com.vnb.banksystem.service.UserService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wxj
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

    @Autowired
    RolePermissionsService permissionsService;
    @Autowired
    UserPermissionsService userPermissionsService;

    @Override
    public Map<String, String> selectByNameAndPassword(Map<String, String> map) {
        return baseMapper.selectByNameAndPassword(map);
    }

    @Override
    public Map<String, String> selectByName(String name) {
        return baseMapper.selectByName(name);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer insertSelective(Map map) {
        FormatDate formatDate = new FormatDate();
        map.put("createTime", formatDate.toString());
        Integer user = baseMapper.insertSelective(map);
        if (user > 0) {
            Map userInfo = baseMapper.selectByName(map.get("name").toString());
            int userId = Integer.parseInt(userInfo.get("id").toString());
            List<Integer> permIds = permissionsService.selectPermIdByRoleId(Integer.parseInt(userInfo.get("roleId").toString()));
            for (Integer ids : permIds) {
                Map permInfo = new HashMap();
                permInfo.put("userId", userId);
                permInfo.put("permId", ids);
                userPermissionsService.insertUserPermission(permInfo);
            }
            return 1;
        }
        return 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer updateUserInfo(Map map) {
        Integer user = baseMapper.updateUserInfo(map);
        if (user > 0) {
            Map userInfo = baseMapper.selectById(Integer.parseInt(map.get("userId").toString()));
            int userId = Integer.parseInt(userInfo.get("id").toString());
            userPermissionsService.deleteByUserId(userId);
            System.out.println(userInfo);
            List<Integer> permIds = permissionsService.selectPermIdByRoleId(Integer.parseInt(userInfo.get("roleId").toString()));
            for (Integer ids : permIds) {
                Map permInfo = new HashMap();
                permInfo.put("userId", userId);
                permInfo.put("permId", ids);
                userPermissionsService.insertUserPermission(permInfo);
            }
            return 1;
        }
        return 0;
    }
}




