package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.domain.UserAccount;
import com.vnb.banksystem.service.UserAccountService;
import com.vnb.banksystem.mapper.UserAccountMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccount>
        implements UserAccountService {

}




