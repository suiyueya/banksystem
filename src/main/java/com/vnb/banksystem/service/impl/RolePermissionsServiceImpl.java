package com.vnb.banksystem.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vnb.banksystem.domain.RolePermissions;
import com.vnb.banksystem.service.RolePermissionsService;
import com.vnb.banksystem.mapper.RolePermissionsMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class RolePermissionsServiceImpl extends ServiceImpl<RolePermissionsMapper, RolePermissions>
        implements RolePermissionsService {

    @Override
    public List<Integer> selectPermIdByRoleId(Integer id) {
        return baseMapper.selectPermIdByRoleId(id);
    }
}




