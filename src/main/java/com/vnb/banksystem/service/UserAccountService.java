package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.UserAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface UserAccountService extends IService<UserAccount> {

}
