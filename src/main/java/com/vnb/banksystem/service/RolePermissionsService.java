package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.RolePermissions;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author wxj
 */
public interface RolePermissionsService extends IService<RolePermissions> {


    /**
     * 根据角色id查询权限id
     *
     * @param id
     * @return
     */
    List<Integer> selectPermIdByRoleId(Integer id);
}
