package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.Transaction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface TransactionService extends IService<Transaction> {

}
