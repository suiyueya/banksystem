package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @author wxj
 */
public interface UserService extends IService<User> {

    /**
     * 根据用户名和密码查询
     *
     * @param map
     * @return
     */
    Map<String, String> selectByNameAndPassword(Map<String, String> map);

    /**
     * 查询用户是否存在
     *
     * @param name
     * @return
     */
    Map<String, String> selectByName(String name);

    /**
     * 新增用户信息
     *
     * @param map
     * @return
     */
    Integer insertSelective(Map map);


    /**
     * 更新用户信息
     *
     * @param map
     * @return
     */
    Integer updateUserInfo(Map map);
}
