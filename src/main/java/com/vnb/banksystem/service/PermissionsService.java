package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.Permissions;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface PermissionsService extends IService<Permissions> {

}
