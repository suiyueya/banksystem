package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface RoleService extends IService<Role> {

}
