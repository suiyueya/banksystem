package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.TransactionDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface TransactionDetailService extends IService<TransactionDetail> {

}
