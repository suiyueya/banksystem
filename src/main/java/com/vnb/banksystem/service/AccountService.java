package com.vnb.banksystem.service;

import com.vnb.banksystem.domain.Account;
import com.baomidou.mybatisplus.extension.service.IService;
import com.vnb.banksystem.domain.vo.AccountInformation;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface AccountService extends IService<Account> {

    //开户
    void addAccount(AccountInformation information);

    //修改账户信息
    void updateAccount(Account account);

    //根据需求查询用户信息
    List<Account> getAccountByDemand(Map map);
}
