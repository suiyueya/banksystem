package com.vnb.banksystem.mapper;

import com.vnb.banksystem.domain.UserAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.vnb.banksystem.domain.UserAccount
 */

@Mapper
public interface UserAccountMapper extends BaseMapper<UserAccount> {

}




