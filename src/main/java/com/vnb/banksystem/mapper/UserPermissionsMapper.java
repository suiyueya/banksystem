package com.vnb.banksystem.mapper;

import com.vnb.banksystem.domain.Permissions;
import com.vnb.banksystem.domain.UserPermissions;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author wxj
 * @Entity com.vnb.banksystem.domain.UserPermissions
 */
@Mapper
public interface UserPermissionsMapper extends BaseMapper<UserPermissions> {


    /**
     * 根据用户id查询权限
     *
     * @param id
     * @return
     */
    List<Permissions> selectUserPermissionsByUserId(Integer id);


    /**
     * 添加用户权限
     *
     * @param map
     * @return
     */
    Integer insertUserPermission(Map map);


    /**
     * 根据用户id删除权限
     *
     * @param id
     * @return
     */
    Integer deleteByUserId(Integer id);
}




