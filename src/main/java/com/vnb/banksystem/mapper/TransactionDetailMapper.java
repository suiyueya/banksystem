package com.vnb.banksystem.mapper;

import com.vnb.banksystem.domain.TransactionDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.vnb.banksystem.domain.TransactionDetail
 */
@Mapper
public interface TransactionDetailMapper extends BaseMapper<TransactionDetail> {

}




