package com.vnb.banksystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vnb.banksystem.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author wxj
 * @Entity com.vnb.banksystem.domain.User
 */

@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户名和密码查询
     *
     * @param map
     * @return
     */
    Map<String, String> selectByNameAndPassword(Map<String, String> map);

    /**
     * 查询用户是否存在
     *
     * @param name
     * @return
     */
    Map<String, String> selectByName(String name);

    /**
     *
     */
    Map selectById(Integer id);

    /**
     * 插入用户信息
     *
     * @param map
     * @return
     */
    Integer insertSelective(Map<String, String> map);

    /**
     * 更新用户信息
     *
     * @param map
     * @return
     */
    Integer updateUserInfo(Map map);
}




