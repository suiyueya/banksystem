package com.vnb.banksystem.mapper;

import com.vnb.banksystem.domain.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.vnb.banksystem.domain.Role
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}




