package com.vnb.banksystem.mapper;

import com.vnb.banksystem.domain.Transaction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.vnb.banksystem.domain.Transaction
 */

@Mapper
public interface TransactionMapper extends BaseMapper<Transaction> {

}




