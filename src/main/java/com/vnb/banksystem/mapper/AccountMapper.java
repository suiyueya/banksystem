package com.vnb.banksystem.mapper;

import com.vnb.banksystem.domain.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Entity com.vnb.banksystem.domain.Account
 */


@Mapper
public interface AccountMapper extends BaseMapper<Account> {

    List<Account> getByDemand(Map map);
}




