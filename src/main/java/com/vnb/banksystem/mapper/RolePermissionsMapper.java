package com.vnb.banksystem.mapper;

import com.vnb.banksystem.domain.RolePermissions;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author wxj
 * @Entity com.vnb.banksystem.domain.RolePermissions
 */
@Mapper
public interface RolePermissionsMapper extends BaseMapper<RolePermissions> {


    /**
     * 根据角色id查询权限id
     *
     * @param id
     * @return
     */
    List<Integer> selectPermIdByRoleId(Integer id);
}




