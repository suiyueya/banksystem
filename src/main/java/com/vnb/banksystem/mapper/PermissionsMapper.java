package com.vnb.banksystem.mapper;

import com.vnb.banksystem.domain.Permissions;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.vnb.banksystem.domain.Permissions
 */
@Mapper
public interface PermissionsMapper extends BaseMapper<Permissions> {

}




