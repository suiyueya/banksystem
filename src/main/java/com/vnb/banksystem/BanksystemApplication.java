package com.vnb.banksystem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wxj
 */
@SpringBootApplication
@MapperScan("com.vnb.banksystem.mapper")
@EnableRabbit
public class BanksystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(BanksystemApplication.class, args);
    }

}
