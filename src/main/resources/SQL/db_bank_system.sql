/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 8.0.27 : Database - db_bank_system
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE = ''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS */`db_bank_system` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `db_bank_system`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account`
(
    `id`          int NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `name`        varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '账户名',
    `password`    varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '交易密码',
    `card_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '银行卡号',
    `bank_name`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '开户行',
    `balance`     decimal(20, 2)                                         DEFAULT '0.00' COMMENT '账户余额',
    `create_time` datetime                                               DEFAULT NULL COMMENT '创建时间',
    `status`      tinyint                                                DEFAULT '0' COMMENT '状态（0->正常，1->冻结，2->销户）',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb3
  ROW_FORMAT = DYNAMIC COMMENT ='账户表';

/*Data for the table `account` */

insert into `account`(`id`, `name`, `password`, `card_number`, `bank_name`, `balance`, `create_time`, `status`)
values (1, 'wxj', '123454', '1111111', '11111', '0.00', '2022-07-08 00:00:00', 0);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions`
(
    `id`         int NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `permission` varchar(50) DEFAULT NULL COMMENT '权限名',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = utf8mb3;

/*Data for the table `permissions` */

insert into `permissions`(`id`, `permission`)
values (1, 'user:add'),
       (2, 'user:selectAll'),
       (3, 'user:update'),
       (4, 'user:delete'),
       (5, 'file:printAll'),
       (6, 'file:print'),
       (7, 'user:select');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role`
(
    `id`   int NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'user' COMMENT '角色名称',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8mb3;

/*Data for the table `role` */

insert into `role`(`id`, `name`)
values (1, 'user'),
       (2, 'admin');

/*Table structure for table `role_permissions` */

DROP TABLE IF EXISTS `role_permissions`;

CREATE TABLE `role_permissions`
(
    `id`      int NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `role_id` int DEFAULT NULL COMMENT '角色id',
    `perm_id` int DEFAULT NULL COMMENT '权限id',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8mb3;

/*Data for the table `role_permissions` */

insert into `role_permissions`(`id`, `role_id`, `perm_id`)
values (1, 1, 7),
       (2, 1, 6),
       (3, 2, 1),
       (4, 2, 2),
       (5, 2, 3),
       (6, 2, 4),
       (7, 2, 5),
       (8, 2, 6),
       (9, 2, 7);

/*Table structure for table `transaction` */

DROP TABLE IF EXISTS `transaction`;

CREATE TABLE `transaction`
(
    `id`               int NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `pay_account_id`   int                                                     DEFAULT NULL COMMENT '付款人账户id',
    `payee_account_id` int                                                     DEFAULT NULL COMMENT '收款人账户id',
    `create_time`      datetime                                                DEFAULT NULL COMMENT '创建时间',
    `status`           tinyint                                                 DEFAULT NULL COMMENT '交易状态（0->成功，1->失败）',
    `remark`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '摘要',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3
  ROW_FORMAT = DYNAMIC COMMENT ='交易表';

/*Data for the table `transaction` */

/*Table structure for table `transaction_detail` */

DROP TABLE IF EXISTS `transaction_detail`;

CREATE TABLE `transaction_detail`
(
    `id`                   int NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `transaction_id`       int                                                     DEFAULT NULL COMMENT '交易id',
    `pay_name`             varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '付款人',
    `pay_card_number`      varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '付款人账户（银行卡号）',
    `pay_bank_name`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '付款人开户行',
    `receiver`             varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '收款人',
    `receiver_card_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '收款人账户（银行卡号）',
    `receiver_bank_name`   varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '收款人开户行',
    `transaction_amount`   decimal(20, 2)                                          DEFAULT NULL COMMENT '交易金额',
    `currency`             varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT 'CNY' COMMENT '币种',
    `deal_type`            varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '交易类型',
    `remark`               varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
    `create_time`          datetime                                                DEFAULT NULL COMMENT '创建时间',
    `status`               tinyint                                                 DEFAULT NULL COMMENT '交易状态（0->成功，1->失败）',
    `cause`                varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '异常原因',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3
  ROW_FORMAT = DYNAMIC COMMENT ='交易明细表';

/*Data for the table `transaction_detail` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `id`                 int NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `name`               varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '用户名',
    `password`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '密码',
    `real_name`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '真实姓名',
    `certificate_type`   varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '证件类型',
    `certificate_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '证件号码',
    `address`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '联系地址',
    `phone`              varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '电话',
    `email`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT '' COMMENT '邮箱',
    `role_id`            int                                                     DEFAULT '1' COMMENT '角色id',
    `create_time`        datetime                                                DEFAULT NULL COMMENT '创建时间',
    `status`             tinyint                                                 DEFAULT '0' COMMENT '状态（0->启用，1->禁用）',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8mb3
  ROW_FORMAT = DYNAMIC COMMENT ='用户表';

/*Data for the table `user` */

insert into `user`(`id`, `name`, `password`, `real_name`, `certificate_type`, `certificate_number`, `address`, `phone`,
                   `email`, `role_id`, `create_time`, `status`)
values (1, 'wxj', '123456', '王晓健', '身份证', '362430200004075700', '广东省深圳市', '15727694441', '269724660@qq.com', 2,
        '2022-07-11 00:00:00', 0),
       (3, 'wxj03', '123456', '', '', '', '', '15727694441', '', 1, '2022-07-14 17:44:21', 0),
       (5, 'wxj04', '123456', '', '', '', '', '15727694441', '', 1, '2022-07-14 17:58:35', 0);

/*Table structure for table `user_account` */

DROP TABLE IF EXISTS `user_account`;

CREATE TABLE `user_account`
(
    `id`          int NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`     int      DEFAULT NULL COMMENT '用户id',
    `account_id`  int      DEFAULT NULL COMMENT '账户id',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3
  ROW_FORMAT = DYNAMIC COMMENT ='用户和账户中间表';

/*Data for the table `user_account` */

/*Table structure for table `user_permissions` */

DROP TABLE IF EXISTS `user_permissions`;

CREATE TABLE `user_permissions`
(
    `id`      int NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id` int DEFAULT NULL COMMENT '用户id',
    `perm_id` int DEFAULT NULL COMMENT '权限id',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 18
  DEFAULT CHARSET = utf8mb3;

/*Data for the table `user_permissions` */

insert into `user_permissions`(`id`, `user_id`, `perm_id`)
values (3, 3, 7),
       (4, 3, 6),
       (5, 4, 7),
       (6, 4, 6),
       (7, 5, 7),
       (8, 5, 6),
       (11, 1, 1),
       (12, 1, 2),
       (13, 1, 3),
       (14, 1, 4),
       (15, 1, 5),
       (16, 1, 6),
       (17, 1, 7);

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
